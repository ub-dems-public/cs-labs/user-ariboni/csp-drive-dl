import numpy as np
from numpy import random
from keras.models import Sequential, Model
from keras.layers import BatchNormalization, Cropping2D, Conv2D, MaxPooling2D, MaxPooling3D, Dense, Dropout, Flatten, \
    Conv3D, ConvLSTM2D, LeakyReLU, Lambda
from keras.callbacks import EarlyStopping, ModelCheckpoint
import matplotlib.pyplot as plt
import keras
import scipy
import cv2
import os
import pickle
import pandas as pd

np.random.seed(42)


def conf_dir(env_key, default_value):
    return os.path.expanduser(os.getenv(env_key, default_value))

data_dir = conf_dir('PC_DATA_DIR', "~/work/cs/csp-drive-dl/data/def")


def read_images(xs):
    x = []
    for i in range(0, len(xs)):
        x.append(cv2.resize(cv2.imread(xs[i])[100:-20], (200, 66)))
    return np.array(x)

def load_images(num_images):
    xs = []
    ys = []

    # read data.txt
    with open(data_dir + "/driving_dataset/data.txt") as f:
        for line in f:
            xs.append(data_dir + "/driving_dataset/data_images/" + line.split()[0])
            # the paper by Nvidia uses the inverse of the turning radius,
            # but steering wheel angle is proportional to the inverse of turning radius
            # so the steering wheel angle in radians is used as the output
            # ys.append(float(line.split()[1]) * scipy.pi / 180)
            line = line.replace(",", " ")
            ys.append(float(line.split()[1]) * scipy.pi / 180)

    xs_ys = list(zip(xs, ys))  # associate each image with its steering angle

    sample = xs_ys[0:num_images]  # sample of images to load
    xs, ys = zip(*sample)

    train_xs = xs[:int(len(xs) * 0.8)]
    train_ys = ys[:int(len(xs) * 0.8)]

    test_xs = xs[-int(len(xs) * 0.2):]
    test_ys = ys[-int(len(xs) * 0.2):]

    valid_xs = train_xs[-int(len(train_xs) * 0.2):]
    valid_ys = train_ys[-int(len(train_ys) * 0.2):]

    train_xs = train_xs[:int(len(train_xs) * 0.8)]
    train_ys = train_ys[:int(len(train_ys) * 0.8)]

    x_train = read_images(train_xs)
    y_train = np.array(train_ys)

    x_valid = read_images(valid_xs)
    y_valid = np.array(valid_ys)

    x_test = read_images(test_xs)
    y_test = np.array(test_ys)

    return x_train, y_train, x_valid, y_valid, x_test, y_test


def create_pilotnet(input_shape):
    model = Sequential()
    model.add(Lambda(lambda x: (x / 255.0), input_shape=(input_shape)))
    model.add(Conv2D(24, kernel_size=(5, 5), activation='elu', strides=(2, 2)))
    model.add(Conv2D(36, kernel_size=(5, 5), activation='elu', strides=(2, 2)))
    model.add(Conv2D(48, kernel_size=(5, 5), activation='elu', strides=(2, 2)))
    model.add(Conv2D(64, kernel_size=(3, 3), activation='elu'))
    model.add(Conv2D(64, kernel_size=(3, 3), activation='elu'))
    model.add(Flatten())
    model.add(Dense(100, activation='elu'))
    model.add(Dense(50, activation='elu'))
    model.add(Dense(10, activation='elu'))
    model.add(Dense(1))

    model.compile(loss='mse', optimizer="adam")
    model.summary()
    return model


def create_plot(history, y_train, y_valid, y_test, y_train_pred, y_valid_pred, y_test_pred):
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    n_epochs = range(len(loss))
    plt.plot(n_epochs, loss, color='mediumseagreen', marker='.', label='training')
    plt.plot(n_epochs, val_loss, color='steelblue', marker='.', label='validation')
    plt.title('Training and validation loss')
    plt.xlabel('epochs')
    plt.ylabel('loss')
    plt.legend(loc="upper right")

    plt.tight_layout()
    plt.savefig(data_dir + '/images/pilotnet_performance.png')

    fig = plt.figure(figsize=(20, 10))
    axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])

    y = np.concatenate([y_train, y_valid, y_test])
    y_pred = np.concatenate([y_train_pred, y_valid_pred, y_test_pred])
    count = range(0, len(y))
    axes.plot(count, y, label="Actual values")
    axes.plot(count, y_pred, label="Predicted values")
    axes.vlines(x=len(y_train), ymin=-6, ymax=6, label='validation set', colors="r")
    axes.vlines(x=(len(y_train) + len(y_valid)), ymin=-6, ymax=6, label='test set')
    axes.legend()
    plt.title("Actual vs Predicted - Pilotnet")
    # plt.show()
    plt.savefig(data_dir + '/images/pilotnet_target.png')


def training_model(num_images=39000, load_weights=False):
    print("Load images...")
    x_train, y_train, x_valid, y_valid, x_test, y_test = load_images(num_images)
    print("Done!")
    model = create_pilotnet(x_train[0].shape)
    model_weights_path = data_dir + "/weights/pilotnet_weights.h5"

    if load_weights == True:
        model.load_weights(model_weights_path)
        print("Results on train set (MSE): ", model.evaluate(x_train, y_train, verbose=0))
        print("Results on validation set (MSE): ", model.evaluate(x_valid, y_valid, verbose=0))
        print("Results on test set (MSE): ", model.evaluate(x_test, y_test, verbose=0))

        y_train_pred = model.predict(x_train)
        y_valid_pred = model.predict(x_valid)
        y_test_pred = model.predict(x_test)

        pd.DataFrame({'actual': y_train, 'prediction':  [i[0] for i in y_train_pred]}, columns=["actual", "prediction"]).to_csv(
            "prediction/pilotnet_train_prediction.csv")
        pd.DataFrame({'actual': y_valid, 'prediction': [i[0] for i in y_valid_pred]}, columns=["actual", "prediction"]).to_csv(
            "prediction/pilotnet_valid_prediction.csv")
        pd.DataFrame({'actual': y_test, 'prediction': [i[0] for i in y_test_pred]}, columns=["actual", "prediction"]).to_csv(
            "prediction/pilotnet_test_prediction.csv")

        return


    history = model.fit(x_train, y_train, shuffle=True, validation_data=(x_valid, y_valid), epochs=15, batch_size=50)#,callbacks=callbacks)
    model.save(model_weights_path)

    y_train_pred = model.predict(x_train)
    y_valid_pred = model.predict(x_valid)
    y_test_pred = model.predict(x_test)
    create_plot(history, y_train, y_valid, y_test, y_train_pred, y_valid_pred, y_test_pred)

    print("Results on train set (MSE): ", model.evaluate(x_train, y_train, verbose=0))
    print("Results on validation set (MSE): ", model.evaluate(x_valid, y_valid, verbose=0))
    print("Results on test set (MSE): ", model.evaluate(x_test, y_test, verbose=0))

    with open('history_pilotnet', 'wb') as file_pi:
        pickle.dump(history.history, file_pi)

if __name__ == "__main__":
    training_model(num_images=39000, load_weights=True)