import os

import pandas as pd
import numpy as np
import statistics
import scipy.misc
from PIL import Image
from skimage.feature import hog
from skimage.color import rgb2grey
from sklearn.utils import resample
from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error
from numpy import random

#creiamo train e test con le rispettive immagini


def conf_dir(env_key, default_value):
    return os.path.expanduser(os.getenv(env_key, default_value))


data_dir = conf_dir('PC_DATA_DIR', "~/work/cs/csp-drive-cl/data/def")


xs = []
ys = []

#read data.txt
with open(data_dir+"/driving_dataset/data.txt") as f:
    for line in f:
        xs.append(data_dir+"/driving_dataset/data_images/" + line.split()[0]) #ci colleghiamo alla directory e aggiungiamo le immagini dato che sono numerate in modo sequenziale
        #the paper by Nvidia uses the inverse of the turning radius,
        #but steering wheel angle is proportional to the inverse of turning radius
        #so the steering wheel angle in radians is used as the output
        #ys.append(float(line.split()[1]) * scipy.pi / 180)
        line=line.replace(",", " ")#modifica al file txt per recuperare lo steering angle
        ys.append(float(line.split()[1]) * scipy.pi / 180) #modifica proposta dall'articolo github

#get number of images
num_images = len(xs)

#shuffle list of images
c = list(zip(xs, ys)) #abbiamo abbinato la repository nel pc per ogni immagine con il corrispondente steering angle
#c=c[0:2000] #prendiamo un sottocampione che ci serve, diaciamo 2000
xs, ys = zip(*c) #riotteniamo le variabili che ci servono

#partizioniamo in train e test
train =  c[:int(len(xs) * 0.8)]
test = c[-int(len(xs) * 0.2):]

#Vogliamo creare una funzione che ci consente di passare da un array tridimensionale ad un array ad una dimensione (flat)
#dove l'array tridimensionale è l'immagine stessa

def create_features(img): #creiamo questa funzione che utilizzeremo nella prossima funzione
    # flatten three channel color image
    color_features = img.flatten() #Restituisce una copia dell'array collassato in una dimensione
    # convert image to greyscale
    grey_image = rgb2grey(img) #trasformiamo l'immagine in bianco e nero
    # get HOG features from greyscale image
    hog_features = hog(grey_image, block_norm='L2-Hys', pixels_per_cell=(16, 16)) #creiamo l'Histogram of oriented gradients
    # combine color and hog features into a single array
    flat_features = np.hstack(color_features)
    return flat_features


# Dopo aver generato un array unidimensionale della singola immagine vogliamo ciclare su tutte le immagini. Creiamo features
# per ogni immagine e poi impiliamo l'array unidimensionale di features che il nostro modello può comprendere

def create_feature_matrix(
        label_dataframe):  # questa funzione ci consente di schiacciare in un unico vettore le informazioni della singola immagine e successivamente di impilarle per creare una matrice
    features_list = []

    for img_id in label_dataframe:
        # load image
        img = Image.open(img_id)  # Image.open perchè non funzionava get_image
        # ridimensiamo l'immagine in BN per ridurre i tempi computazionali
        reshaped_img = scipy.misc.imresize(img, [66, 200]) / 255.0  # reshape e normalizzazione
        # get features for image
        image_features = create_features(reshaped_img)
        features_list.append(image_features)

    # convert list of arrays into a matrix
    feature_matrix = np.array(features_list)
    return feature_matrix


# run create_feature_matrix on our dataframe of images
feature_matrix = create_feature_matrix(np.array(train)[:, 0])

# creiamo il ricampionamento bootstrap
np.random.seed(42)  # impostiamo un seme per rendere paragonabili gli output

bootstrap_score = []

B = 10  # ripetizioni bootstrap
for rip in range(0, B):

    boot_x = resample(pd.DataFrame(feature_matrix), replace=True,
                      n_samples=len(train))  # ricampioniamo la matrice ottenuta dalla funzione
    oob_ind = [x for x in pd.DataFrame(feature_matrix).index if
               x not in boot_x.index]  # otteniamo gli iondici delle osservazioni out of bag
    oob_x = pd.DataFrame(feature_matrix).loc[oob_ind]  # ooteniamo le osservazioni out of bag della matrice
    boot_y = []
    for ind in boot_x.index:
        boot_y.append(float(np.array(train)[ind, 1]))  # otteniamo le oss bootstappate della variabile target
    oob_y = []
    for ind in oob_x.index:
        oob_y.append(float(np.array(train)[ind, 1]))  # otteniamo le oss oob della variabile target

    regressor = SVR(kernel='rbf')
    regressor.fit(boot_x, boot_y)  # adattiamo il modello alle osservazioni bootrappate

    y_pred = regressor.predict(oob_x)  # facciamo la previsione sulle osservazioni bootstrappate

    mse = mean_squared_error(oob_y, y_pred)  # calcoliamo il mse
    bootstrap_score.append(mse)

print(statistics.mean(bootstrap_score))

print(np.var(bootstrap_score))

print(bootstrap_score)

#Vediamo i risultati anche sul test

#Trasformiamo i dati di test affinchè siano comprensibili al modello
X_test = create_feature_matrix(np.array(test)[:,0])

#trasformiamo la target variable
y_test=[]
for ind in range (0, len(test)):
     y_test.append(float(np.array(test)[ind,1]))#necessaria per essere capita dal modello

#facciamo le previsioni sul test con il modello addestrato
y_pred_test = regressor.predict(X_test)
mse = mean_squared_error(y_test, y_pred_test)
print(mse)