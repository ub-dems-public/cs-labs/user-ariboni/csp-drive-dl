import os

import numpy as np
from matplotlib import pyplot as plt
#%matplotlib inline
from scipy.misc import toimage
import scipy.misc
import random
from sklearn.utils import resample

import keras
import keras.models as models

from keras.models import Sequential, Model
from keras.layers.core import Dense, Dropout, Activation, Flatten, Reshape
from keras.layers import BatchNormalization,Input, Cropping2D
from keras.layers.recurrent import SimpleRNN, LSTM
from keras.layers.convolutional import Conv2D
from keras.optimizers import SGD, Adam, RMSprop
import sklearn.metrics as metrics
from keras.layers import MaxPooling2D

import GPy, GPyOpt
import numpy as np
import pandas as pds
import random
from keras.layers import Activation, Dropout, BatchNormalization, Dense
from keras.models import Sequential
from keras.datasets import mnist
from keras.metrics import categorical_crossentropy
from keras.utils import np_utils
from keras.optimizers import Adam
from keras.callbacks import EarlyStopping

import keras
import keras.models as models

from keras.models import Sequential, Model
from keras.layers.core import Dense, Dropout, Activation, Flatten, Reshape
from keras.layers import BatchNormalization,Input
from keras.layers.recurrent import SimpleRNN, LSTM
from keras.layers.convolutional import Convolution2D
from keras.optimizers import SGD, Adam, RMSprop
import sklearn.metrics as metrics
from keras.layers import MaxPooling2D
from keras.layers import LeakyReLU
from keras.layers.convolutional_recurrent import ConvLSTM2D
from keras.layers import Conv3D, AveragePooling3D, Reshape


#creiamo train e test con le rispettive immagini


def conf_dir(env_key, default_value):
    return os.path.expanduser(os.getenv(env_key, default_value))


data_dir = conf_dir('PC_DATA_DIR', "~/work/cs/csp-drive-cl/data/def")


xs = []
ys = []

#read data.txt
with open(data_dir+"/driving_dataset/data.txt") as f:
    for line in f:
        xs.append(data_dir+"/driving_dataset/data_images/" + line.split()[0]) #ci colleghiamo alla directory e aggiungiamo le immagini dato che sono numerate in modo sequenziale
        #the paper by Nvidia uses the inverse of the turning radius,
        #but steering wheel angle is proportional to the inverse of turning radius
        #so the steering wheel angle in radians is used as the output
        #ys.append(float(line.split()[1]) * scipy.pi / 180)
        line=line.replace(",", " ") #modifica al file txt per recuperare lo steering angle
        ys.append(float(line.split()[1]) * scipy.pi / 180) #modifica proposta dall'articolo github

#get number of images
num_images = len(xs)

#shuffle list of images
c = list(zip(xs, ys)) #abbiamo abbinato la repository nel pc per ogni immagine con il corrispondente steering angle
#c=c[0:1000] #prendiamo un sottocampione che ci serve, diaciamo 2000
xs, ys = zip(*c) #riotteniamo le variabili che ci servono

#partizioniamo in train e test
train =  c[:int(len(xs) * 0.8)]
test = c[-int(len(xs) * 0.2):]


# DRIVE class
class DRIVE():
    def __init__(self,  # first_input=784, last_output=10,
                 l1_out=64,
                 l2_out=64,
                 l3_out=64,
                 l4_out=8,
                 l5_out=3,
                 l6_out=100,
                 l1_drop=0.5,
                 batch_size=100,
                 epochs=30,
                 validation_split=0.2):
        # self.__first_input = first_input
        # self.__last_output = last_output
        self.l1_out = l1_out
        self.l2_out = l2_out
        self.l3_out = l3_out
        self.l4_out = l4_out
        self.l5_out = l5_out
        self.l6_out = l6_out
        self.l1_drop = l1_drop
        self.batch_size = batch_size
        self.epochs = epochs
        self.validation_split = validation_split
        self.__model = self.drive_model()

    def drive_model(self):
        model = Sequential()

        model.add(ConvLSTM2D(self.l1_out, kernel_size=(3, 3), input_shape=(1, 66, 200, 3), padding='same',
                             return_sequences=True, activation='relu'))
        model.add(BatchNormalization())
        model.add(ConvLSTM2D(self.l2_out, kernel_size=(3, 3), padding='same', return_sequences=True, activation='relu'))
        model.add(BatchNormalization())
        model.add(ConvLSTM2D(self.l3_out, kernel_size=(3, 3), padding='same', return_sequences=True, activation='relu'))
        model.add(BatchNormalization())
        model.add(ConvLSTM2D(self.l4_out, kernel_size=(3, 3), padding='same', return_sequences=True, activation='relu'))
        model.add(BatchNormalization())
        # model.add(Dropout(0.25))

        model.add(Conv3D(self.l5_out, kernel_size=(3, 3, 3),
                         # activation='sigmoid',
                         padding='same', data_format='channels_last'))
        # model.add(AveragePooling3D((1, 66,200)))
        # model.add(Convolution2D(64,kernel_size=(3, 3),border_mode='valid', activation='relu', subsample=(1,1)))
        model.add(Flatten())
        model.add(Dense(self.l6_out, activation='relu'))  # 512
        model.add(LeakyReLU(-0.2))  # -0.2
        model.add(Dropout(self.l1_drop))
        model.add(Dense(1, activation='relu'))

        model.compile(loss='mse', optimizer=Adam())

        return model

    # fit mnist model
    def drive_fit(self):
        early_stopping = EarlyStopping(patience=0, verbose=1)

        self.__model.fit(x_boot, y_boot,
                         batch_size=self.batch_size,
                         epochs=self.epochs,
                         verbose=1,
                         validation_split=self.validation_split,
                         callbacks=[early_stopping])

    # evaluate mnist model
    def drive_evaluate(self):
        self.drive_fit()

        evaluation = self.__model.evaluate(x_oob, y_oob, batch_size=self.batch_size, verbose=1)
        return evaluation

# function to run mnist class
def run_drive(l1_out=64, l2_out=64, l3_out=64,l4_out=8,l5_out=3, l6_out=100,l1_drop=0.5, batch_size=100, epochs=30, validation_split=0.2):#first_input=784, last_output=10,
#l5_out=3
    _drive = DRIVE(l1_out=l1_out, l2_out=l2_out, l3_out=l3_out, l4_out=l4_out, l5_out=l5_out, l6_out=l6_out,l1_drop=l1_drop,
                   batch_size=batch_size, epochs=epochs,
                   validation_split=validation_split)#first_input=first_input, last_output=last_output,
    drive_evaluation = _drive.drive_evaluate()
    return drive_evaluation

# bounds for hyper-parameters in mnist model
# the bounds dict should be in order of continuous type and then discrete type
bounds = [{'name': 'validation_split', 'type': 'continuous',  'domain': (0.0, 0.3)},
          {'name': 'l1_drop',          'type': 'continuous',  'domain': (0.0, 0.5)},
          {'name': 'l1_out',           'type': 'discrete',    'domain': (8,16,32,40, 64)},
          {'name': 'l2_out',           'type': 'discrete',    'domain': (8,16,32,40, 64)},
          {'name': 'l3_out',           'type': 'discrete',    'domain': (8,16,32, 40, 64)},
          {'name': 'l4_out',           'type': 'discrete',    'domain': (1,2,4, 6, 8)},
          {'name': 'l5_out',           'type': 'discrete',    'domain': (1,2,3)},
          {'name': 'l6_out',           'type': 'discrete',    'domain': (32, 64,100, 200, 512)},#128
          {'name': 'batch_size',       'type': 'discrete',    'domain': (10, 100, 500)},
          {'name': 'epochs',           'type': 'discrete',    'domain': (10, 20, 30)}]

# function to optimize mnist model
def f(x):
    print(x)
    evaluation = run_drive(
        l1_drop = float(x[:,1]),
        l1_out = int(x[:,2]),
        l2_out = int(x[:,3]),
        l3_out = int(x[:,4]),
        l4_out = int(x[:,5]),
        l5_out = int(x[:,6]),
        l6_out = int(x[:,7]),
        batch_size = int(x[:,8]),
        epochs = int(x[:,9]),
        validation_split = float(x[:,0]))
    print("LOSS:\t{0}".format(evaluation))
    print(evaluation)
    return evaluation


# procediamo con il bootstrap
np.random.seed(42)  # impostiamo un seme per risultati comparabili

opt_hyperparameters = []
opt_loss = []

B = 5  # numero di bootstrap
for rip in range(0, B):
    boot = resample(train, replace=True, n_samples=len(train))  # ricampioniamo le righe del dataset
    oob = [x for x in train if x not in boot]  # otteniamo le oob che non fanno parte delle oss campionate

    x_boot = []
    y_boot = []
    for i in range(0, len(boot)):
        x_boot.append(scipy.misc.imresize(scipy.misc.imread(np.array(boot)[i, 0]), [66, 200]) / 255.0)  # carichiamo le immagini per il boot con normalizzazione e reshape
        y_boot.append(np.array(boot)[i, 1])  # carichiamo la variabile target per il boot
    # (66,200)
    x_oob = []
    y_oob = []
    for i in range(0, len(oob)):  # analogo a sopra ma con oob
        x_oob.append(scipy.misc.imresize(scipy.misc.imread(np.array(oob)[i, 0]), [66, 200]) / 255.0)
        y_oob.append(np.array(oob)[i, 1])

    x_boot = np.array(x_boot)
    y_boot = np.array(y_boot)

    x_oob = np.array(x_oob)
    y_oob = np.array(y_oob)

    x_boot = x_boot.reshape(len(boot), 1, 66, 200, 3)
    x_oob = x_oob.reshape(len(oob), 1, 66, 200, 3)

    opt_drive = GPyOpt.methods.BayesianOptimization(f=f, domain=bounds, model_type="GP", acquisition_type="EI")
    opt_drive.run_optimization(max_iter=10)  # 100

    opt_hyperparameters.append(opt_drive.x_opt)
    opt_loss.append(opt_drive.fx_opt)

print(opt_hyperparameters)
print(opt_loss)



