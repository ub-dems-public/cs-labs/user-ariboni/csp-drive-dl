# importiamo i pacchetti che ci servono
import os

import numpy as np
import scipy.misc
import statistics
from sklearn.utils import resample
from numpy import random
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Flatten
from keras.layers import BatchNormalization, Cropping2D
from keras.layers.convolutional import Conv2D
from keras.layers import MaxPooling2D


#creiamo train e test con le rispettive immagini

def conf_dir(env_key, default_value):
    return os.path.expanduser(os.getenv(env_key, default_value))


data_dir = conf_dir('PC_DATA_DIR', "~/work/cs/csp-drive-cl/data/def")

xs = []
ys = []

#read data.txt
with open(data_dir+"/driving_dataset/data.txt") as f:
    for line in f:
        xs.append(data_dir+"/driving_dataset/data_images/" + line.split()[0]) #ci colleghiamo alla directory e aggiungiamo le immagini dato che sono numerate in modo sequenziale
        #the paper by Nvidia uses the inverse of the turning radius,
        #but steering wheel angle is proportional to the inverse of turning radius
        #so the steering wheel angle in radians is used as the output
        #ys.append(float(line.split()[1]) * scipy.pi / 180)
        line=line.replace(",", " ") #modifica al file txt per recuperare lo steering angle
        ys.append(float(line.split()[1]) * scipy.pi / 180) #modifica proposta dall'articolo github

#get number of images
num_images = len(xs)

#shuffle list of images
c = list(zip(xs, ys)) #abbiamo abbinato la repository nel pc per ogni immagine con il corrispondente steering angle
#c=c[0:2000] #prendiamo un sottocampione che ci serve, diaciamo 2000
xs, ys = zip(*c) #riotteniamo le variabili che ci servono

#partizioniamo in train e test
train =  c[:int(len(xs) * 0.8)]
test = c[-int(len(xs) * 0.2):]

#creiamo il modello j_net mediante keras


model = Sequential()

model.add(BatchNormalization(epsilon=0.001,mode=0, axis=1,input_shape=(66, 200, 3)))
model.add(Cropping2D(cropping=((6,0),(0,0))))

model.add(Conv2D(16, (5, 5), padding="same", activation="relu"))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(32, (5, 5), padding="same", activation="relu"))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, (5, 5), padding="same", activation="relu"))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())
model.add(Dropout(0.25))
model.add(Dense(10))
model.add(Dense(1))

model.compile(loss='mse', optimizer="adam") #come ottimizzatore abbiamo impostato adam e come pedita mse
model.summary()

# procediamo con il bootstrap
np.random.seed(42)  # impostiamo un seme per risultati comparabili

bootstrap_score = []

B = 10  # numero di bootstrap
for rip in range(0, B):
    boot = resample(train, replace=True, n_samples=len(train))  # ricampioniamo le righe del dataset
    oob = [x for x in train if x not in boot]  # otteniamo le oob che non fanno parte delle oss campionate

    x_boot = []
    y_boot = []
    for i in range(0, len(boot)):
        x_boot.append(scipy.misc.imresize(scipy.misc.imread(np.array(boot)[i, 0]), [66, 200]) / 255.0)  # carichiamo le immagini per il boot con normalizzazione e reshape
        y_boot.append(np.array(boot)[i, 1])  # carichiamo la variabile target per il boot

    x_oob = []
    y_oob = []
    for i in range(0, len(oob)):  # analogo a sopra ma con oob
        x_oob.append(scipy.misc.imresize(scipy.misc.imread(np.array(oob)[i, 0]), [66, 200]) / 255.0)
        y_oob.append(np.array(oob)[i, 1])

    x_boot = np.array(x_boot)
    y_boot = np.array(y_boot)

    x_oob = np.array(x_oob)
    y_oob = np.array(y_oob)

    history = model.fit(x_boot, y_boot, batch_size=100, epochs=30, verbose=1,
                        validation_split=0.2)  # adattiamo sui dati
    score = model.evaluate(x_oob, y_oob, verbose=1)
    bootstrap_score.append(score)
# batch size 100 e 30 epoche le manteniamo per tutte le reti

print(statistics.mean(bootstrap_score))

print(np.var(bootstrap_score))

print(bootstrap_score)

#valutiamo la rete anche sul test
x_test=[]
y_test=[]
for i in range(0, len(test)): #analogo a sopra ma con oob
    x_test.append(scipy.misc.imresize(scipy.misc.imread(np.array(test)[i,0]), [66, 200]) / 255.0)
    y_test.append(np.array(test)[i,1])
x_test=np.array(x_test)
y_test=np.array(y_test)

test_score  = model.evaluate(x_test, y_test, verbose=1)
print(test_score)