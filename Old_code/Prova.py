# importiamo i pacchetti che ci servono
import os

import numpy as np
import scipy.misc
import statistics
from sklearn.utils import resample
from numpy import random
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Flatten
from keras.layers import BatchNormalization, Cropping2D
from keras.layers.convolutional import Conv2D
from keras.layers import MaxPooling2D


#creiamo train e test con le rispettive immagini


def conf_dir(env_key, default_value):
    return os.path.expanduser(os.getenv(env_key, default_value))


data_dir = conf_dir('PC_DATA_DIR', "~/work/cs/csp-drive-cl/data/def")


xs = []
ys = []

#read data.txt
with open(data_dir+"/driving_dataset/data.txt") as f:
    for line in f:
        xs.append(data_dir+"/driving_dataset/data_images/" + line.split()[0]) #ci colleghiamo alla directory e aggiungiamo le immagini dato che sono numerate in modo sequenziale
        #the paper by Nvidia uses the inverse of the turning radius,
        #but steering wheel angle is proportional to the inverse of turning radius
        #so the steering wheel angle in radians is used as the output
        #ys.append(float(line.split()[1]) * scipy.pi / 180)
        line=line.replace(",", " ") #modifica al file txt per recuperare lo steering angle
        ys.append(float(line.split()[1]) * scipy.pi / 180) #modifica proposta dall'articolo github

#get number of images
num_images = len(xs)

#shuffle list of images
c = list(zip(xs, ys)) #abbiamo abbinato la repository nel pc per ogni immagine con il corrispondente steering angle
#c=c[0:2000] #prendiamo un sottocampione che ci serve, diaciamo 2000
xs, ys = zip(*c) #riotteniamo le variabili che ci servono

#partizioniamo in train e test
train =  c[:int(len(xs) * 0.8)]
test = c[-int(len(xs) * 0.2):]

#creiamo il modello j_net mediante keras


model = Sequential()

model.add(BatchNormalization(epsilon=0.001,mode=0, axis=1,input_shape=(66, 200, 3)))
model.add(Cropping2D(cropping=((6,0),(0,0))))

model.add(Conv2D(16, (5, 5), padding="same", activation="relu"))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(32, (5, 5), padding="same", activation="relu"))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, (5, 5), padding="same", activation="relu"))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())
model.add(Dropout(0.25))
model.add(Dense(10))
model.add(Dense(1))

model.compile(loss='mse', optimizer="adam") #come ottimizzatore abbiamo impostato adam e come pedita mse
model.summary()