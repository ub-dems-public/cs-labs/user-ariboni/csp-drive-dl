import numpy as np
import scipy.misc
from numpy import random
from keras.models import Sequential
from keras.layers.core import Dense, Flatten
from keras.layers.convolutional import Convolution2D

np.random.seed(42)

# creiamo train e test con le rispettive immagini

def conf_dir(env_key, default_value):
    return os.path.expanduser(os.getenv(env_key, default_value))


data_dir = conf_dir('PC_DATA_DIR', "~/work/cs/csp-drive-cl/data/def")


xs = []
ys = []

#read data.txt
with open(data_dir+"/driving_dataset/data.txt") as f:
    for line in f:
        xs.append(data_dir+"/driving_dataset/data_images/" + line.split()[0]) 
	# ci colleghiamo alla directory e aggiungiamo le immagini dato che sono numerate in modo sequenziale
        # the paper by Nvidia uses the inverse of the turning radius,
        # but steering wheel angle is proportional to the inverse of turning radius
        # so the steering wheel angle in radians is used as the output
        # ys.append(float(line.split()[1]) * scipy.pi / 180)
        line = line.replace(",", " ")
        ys.append(float(line.split()[1]) * scipy.pi / 180)

# get number of images
num_images = len(xs)

# shuffle list of images
c = list(zip(xs, ys))  # abbiamo abbinato la repository nel pc per ogni immagine con il corrispondente steering angle
c=c[0:39000]
xs, ys = zip(*c)

train_xs=xs[:int(len(xs) * 0.8)]
train_ys=ys[:int(len(xs) * 0.8)]

test_xs = xs[-int(len(xs) * 0.2):]  # creazione del validation set
test_ys = ys[-int(len(xs) * 0.2):]

x_train = []
y_train = []
for i in range(0, len(train_xs)):
    x_train.append(scipy.misc.imresize(scipy.misc.imread(train_xs[i]), [66, 100]) / 255.0)  # reshape e normalizzazione
    y_train.append(np.array(train_ys[i]))

x_train = np.array(x_train)
y_train = np.array(y_train)

x_test = []
y_test = []
for i in range(0, len(test_xs)):
    x_test.append(scipy.misc.imresize(scipy.misc.imread(test_xs[i]), [66, 100]) / 255.0)  # reshape e normalizzazione
    y_test.append(test_ys[i])

x_test = np.array(x_test)
y_test = np.array(y_test)




#creiamo la PilotNet mediante Keras

model = Sequential()
model.add(Convolution2D(24,kernel_size=(5, 5), activation='elu', subsample=(2,2), input_shape=(66,100,3)))
model.add(Convolution2D(36,kernel_size=(5, 5), activation='elu', subsample=(2,2)))
model.add(Convolution2D(48,kernel_size=(5, 5), activation='elu', subsample=(2,2)))
model.add(Convolution2D(64,kernel_size=(3, 3), activation='elu'))
model.add(Convolution2D(64,kernel_size=(3, 3), activation='elu'))
model.add(Flatten())
model.add(Dense(100, activation='elu'))
model.add(Dense(50, activation='elu'))
model.add(Dense(10, activation='elu'))
model.add(Dense(1))

model.compile(loss='mse', optimizer="adam") #come ottimizzatore abbiamo impostato adam e come pedita mse
model.summary()



history = model.fit(x_train, y_train, batch_size=100, epochs=15, verbose=1, validation_split=0.2)  # adattiamo sui dati
score = model.evaluate(x_test, y_test, verbose=1)
# batch size 100 e 30 epoche le manteniamo per tutte le reti
print(score)