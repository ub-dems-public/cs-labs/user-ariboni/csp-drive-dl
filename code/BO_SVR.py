import pandas as pd
import numpy as np
import scipy.misc
from PIL import Image
from skimage.feature import hog
from skimage.color import rgb2grey
from sklearn.model_selection import train_test_split
from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error
import GPyOpt
import GPy
import time

start_time = time.time()

#creiamo train e test con le rispettive immagini
np.random.seed(42)

def conf_dir(env_key, default_value):
    return os.path.expanduser(os.getenv(env_key, default_value))


data_dir = conf_dir('PC_DATA_DIR', "~/work/cs/csp-drive-cl/data/def")


xs = []
ys = []

#read data.txt
with open(data_dir+"/driving_dataset/data.txt") as f:
    for line in f:
        xs.append(data_dir+"/driving_dataset/data_images/" + line.split()[0]) 
	# ci colleghiamo alla directory e aggiungiamo le immagini dato che sono numerate in modo sequenziale
        # the paper by Nvidia uses the inverse of the turning radius,
        # but steering wheel angle is proportional to the inverse of turning radius
        # so the steering wheel angle in radians is used as the output
        # ys.append(float(line.split()[1]) * scipy.pi / 180)
        line = line.replace(",", " ")
        ys.append(float(line.split()[1]) * scipy.pi / 180)

#shuffle list of images
c = list(zip(xs, ys))
c=c[0:12000]
#abbiamo abbinato la repository nel pc per ogni immagine con il corrispondente steering angle
#c=c[0:2000] #prendiamo un sottocampione che ci serve, diaciamo 2000
xs, ys = zip(*c) #riotteniamo le variabili che ci servono

#partizioniamo in train e test
train = c[:int(len(xs) * 0.8)]#0.8
test = c[-int(len(xs) * 0.2):]
print(len(train))

#Vogliamo creare una funzione che ci consente di passare da un array tridimensionale ad un array ad una dimensione (flat)
#dove l'array tridimensionale Ã¨ l'immagine stessa

def create_features(img): #creiamo questa funzione che utilizzeremo nella prossima funzione
    # flatten three channel color image
    color_features = img.flatten() #Restituisce una copia dell'array collassato in una dimensione
    # convert image to greyscale
    grey_image = rgb2grey(img) #trasformiamo l'immagine in bianco e nero
    # get HOG features from greyscale image
    hog_features = hog(grey_image, block_norm='L2-Hys', pixels_per_cell=(16, 16)) #creiamo l'Histogram of oriented gradients
    # combine color and hog features into a single array
    flat_features = np.hstack(color_features)
    return flat_features

print("return flat_features")
# Dopo aver generato un array unidimensionale della singola immagine vogliamo ciclare su tutte le immagini. Creiamo features
# per ogni immagine e poi impiliamo l'array unidimensionale di features che il nostro modello puÃ² comprendere

def create_feature_matrix(
        label_dataframe):  # questa funzione ci consente di schiacciare in un unico vettore le informazioni della singola immagine e successivamente di impilarle per creare una matrice
    features_list = []

    for img_id in label_dataframe:
        # load image
        img = Image.open(img_id)  # Image.open perchÃ¨ non funzionava get_image
        # ridimensiamo l'immagine in BN per ridurre i tempi computazionali
        reshaped_img = scipy.misc.imresize(img, [66, 100]) / 255.0  # reshape e normalizzazione
        # get features for image
        image_features = create_features(reshaped_img)
        features_list.append(image_features)

    # convert list of arrays into a matrix
    feature_matrix = np.array(features_list)
    return feature_matrix

print("return feature_matrix")

# run create_feature_matrix on our dataframe of images
feature_matrix = create_feature_matrix(np.array(train)[:, 0])
target = np.array(train)[:, 1]
print("feature map")
# creiamo il ricampionamento bootstrap

X=pd.DataFrame(feature_matrix)
y =pd.Series(target)
X_train, X_val, y_train, y_val = train_test_split(X,
                                                    y,
                                                    test_size=.2)


# MNIST class
class DRIVE():
    def __init__(self,
                 l1_kernel='sigmoid', l2_degree=3, l3_gamma='scale', l4_coef0=0.0, l5_tol=0.001, l6_C=1.0,
                 l7_epsilon=0.1, l8_shrinking=True, l9_cache_size=200):
        self.l1_kernel = l1_kernel
        self.l2_degree = l2_degree
        self.l3_gamma = l3_gamma
        self.l4_coef0 = l4_coef0
        self.l5_tol = l5_tol
        self.l6_C = l6_C
        self.l7_epsilon = l7_epsilon
        self.l8_shrinking = l8_shrinking
        self.l9_cache_size = l9_cache_size
        # self.validation_split = validation_split
        self.__model = self.drive_model()

    #  model
    def drive_model(self):
        model = SVR(self.l1_kernel, self.l2_degree, self.l3_gamma, self.l4_coef0, self.l5_tol, self.l6_C,
                    self.l7_epsilon, self.l8_shrinking, self.l9_cache_size)
        model.fit(X_train, y_train)

        return model

    # fit mnist model
    def drive_fit(self):
        # early_stopping = EarlyStopping(patience=0, verbose=1)

        self.__model.fit(X_val, y_val)

    # evaluate mnist model
    def drive_evaluate(self):
        self.drive_fit()

        y_pred = self.__model.predict(X_val)
        evaluation = mean_squared_error(y_val, y_pred)
        return evaluation


def run_drive(l1_kernel='sigmoid', l2_degree=3, l3_gamma='scale', l4_coef0=0.0, l5_tol=0.001, l6_C=1.0,
                 l7_epsilon=0.1, l8_shrinking=True, l9_cache_size=200):
    _drive = DRIVE(l1_kernel=l1_kernel, l2_degree=l2_degree, l3_gamma=l3_gamma, l4_coef0=l4_coef0, l5_tol=l5_tol,
                   l6_C=l6_C,l7_epsilon=l7_epsilon, l8_shrinking=l8_shrinking, l9_cache_size=l9_cache_size)
    drive_evaluation = _drive.drive_evaluate()
    return drive_evaluation


bounds = [{'name': 'l4_coef0', 'type': 'continuous', 'domain': (1.0, 10.0)},
          {'name': 'l5_tol', 'type': 'continuous', 'domain': (0.001, 1.0)},
          {'name': 'l6_C', 'type': 'continuous', 'domain': (1.0, 5.0)},
          {'name': 'l7_epsilon', 'type': 'continuous', 'domain': (0.001, 1.0)},
          {'name': 'l2_degree', 'type': 'discrete', 'domain': (1, 2, 3, 4, 5)}]

# {'name': 'l1_kernel', 'type': 'categorical',  'domain': ('linear', 'poly', 'rbf', 'sigmoid')},
# {'name': 'l8_shrinking', 'type': 'categorical',    'domain': ("True", "False")},


# function to optimize mnist model
def f(x):
    print(x)
    evaluation = run_drive(
        l4_coef0=float(x[:,0]),
        l5_tol=float(x[:,1]),
        l6_C = float(x[:,2]),
        l7_epsilon = float(x[:,3]),
        l2_degree = int(x[:,4]) )
    print("LOSS:\t{0}".format(evaluation))
    print(evaluation)
    return evaluation


opt_drive = GPyOpt.methods.BayesianOptimization(f=f, domain=bounds, model_type="GP", acquisition_type="EI")

# optimize mnist model
# Modified by Antonio to save BO results (2020-04-18)
# opt_drive.run_optimization(max_iter=10)
opt_drive.run_optimization(max_iter=10, report_file="BO_SVR_rpt.txt", evaluations_file="BO_SVR_evals.txt")

print("parametri ottimizzati")
print(opt_drive.x_opt)

print(opt_drive.fx_opt)

print(opt_drive.x_opt[0])
print(opt_drive.x_opt[1])
print(opt_drive.x_opt[2])
print(opt_drive.x_opt[3])
print(opt_drive.x_opt[4])

print("--- %s seconds ---" % (time.time() - start_time))

#regressor = SVR(kernel='poly', degree=2, gamma='scale', coef0=9.551, tol=0.305, C=2.9724, epsilon=0.001)
