# DRIVE class
class DRIVE():
    def __init__(self,  # first_input=784, last_output=10,
                 l1_out=64,
                 l2_out=64,
                 l3_out=64,
                 l4_out=8,
                 l5_out=3,
                 l6_out=100,
                 l1_drop=0.5,
                 batch_size=100,
                 epochs=30,
                 validation_split=0.2):
        # self.__first_input = first_input
        # self.__last_output = last_output
        self.l1_out = l1_out
        self.l2_out = l2_out
        self.l3_out = l3_out
        self.l4_out = l4_out
        self.l5_out = l5_out
        self.l6_out = l6_out
        self.l1_drop = l1_drop
        self.batch_size = batch_size
        self.epochs = epochs
        self.validation_split = validation_split
        self.__model = self.drive_model()

    def drive_model(self):
        model = Sequential()

        model.add(ConvLSTM2D(self.l1_out, kernel_size=(3, 3), input_shape=(1, 32, 32, 3), padding='same',
                             return_sequences=True, activation='relu'))
        model.add(BatchNormalization())
        model.add(ConvLSTM2D(self.l2_out, kernel_size=(3, 3), padding='same', return_sequences=True, activation='relu'))
        model.add(BatchNormalization())
        model.add(ConvLSTM2D(self.l3_out, kernel_size=(3, 3), padding='same', return_sequences=True, activation='relu'))
        model.add(BatchNormalization())
        model.add(ConvLSTM2D(self.l4_out, kernel_size=(3, 3), padding='same', return_sequences=True, activation='relu'))
        model.add(BatchNormalization())
        # model.add(Dropout(0.25))

        model.add(Conv3D(self.l5_out, kernel_size=(3, 3, 3),
                         # activation='sigmoid',
                         padding='same', data_format='channels_last'))
        # model.add(AveragePooling3D((1, 66,200)))
        # model.add(Convolution2D(64,kernel_size=(3, 3),border_mode='valid', activation='relu', subsample=(1,1)))
        model.add(Flatten())
        model.add(Dense(self.l6_out, activation='relu'))  # 512
        model.add(LeakyReLU(-0.2))  # -0.2
        model.add(Dropout(0.5))
        model.add(Dense(1, activation='relu'))

        model.compile(loss='mse', optimizer=Adam())

        return model

    # fit mnist model
    def drive_fit(self):
        early_stopping = EarlyStopping(patience=0, verbose=1)

        self.__model.fit(x_train, y_train,
                         batch_size=self.batch_size,
                         epochs=self.epochs,
                         verbose=1,
                         validation_split=self.validation_split,
                         callbacks=[early_stopping])

    # evaluate mnist model
    def drive_evaluate(self):
        self.drive_fit()

        evaluation = self.__model.evaluate(x_test, y_test, batch_size=self.batch_size, verbose=1)
        return evaluation

# function to run mnist class
def run_drive(l1_out=64, l2_out=64, l3_out=64,l4_out=8,l5_out=3, l6_out=100,l1_drop=0.5, batch_size=100, epochs=30, validation_split=0.2):#first_input=784, last_output=10,
#l5_out=3
    _drive = DRIVE(l1_out=l1_out, l2_out=l2_out, l3_out=l3_out, l4_out=l4_out, l5_out=l5_out, l6_out=l6_out,l1_drop=l1_drop,
                   batch_size=batch_size, epochs=epochs,
                   validation_split=validation_split)#first_input=first_input, last_output=last_output,
    drive_evaluation = _drive.drive_evaluate()
    return drive_evaluation

# bounds for hyper-parameters in mnist model
# the bounds dict should be in order of continuous type and then discrete type
bounds = [{'name': 'validation_split', 'type': 'continuous',  'domain': (0.0, 0.3)},
          {'name': 'l1_drop',          'type': 'continuous',  'domain': (0.0, 0.3)},
          {'name': 'l1_out',           'type': 'discrete',    'domain': (8,16,32,40, 64)},
          {'name': 'l2_out',           'type': 'discrete',    'domain': (8,16,32,40, 64)},
          {'name': 'l3_out',           'type': 'discrete',    'domain': (8,16,32, 40, 64)},
          {'name': 'l4_out',           'type': 'discrete',    'domain': (1,2,4, 6, 8)},
          {'name': 'l5_out',           'type': 'discrete',    'domain': (1,2,3)},
          {'name': 'l6_out',           'type': 'discrete',    'domain': (32, 64,100, 200, 512)},#128
          {'name': 'batch_size',       'type': 'discrete',    'domain': (10, 100, 500)},
          {'name': 'epochs',           'type': 'discrete',    'domain': (10, 20, 30)}]

opt_drive = GPyOpt.methods.BayesianOptimization(f=f, domain=bounds, model_type="GP", acquisition_type="EI")
# optimize mnist model
# modified by Antonio to save BO's results (2020-04-18)
#opt_drive.run_optimization(max_iter=10)#100
opt_drive.run_optimization(max_iter=10, report_file="BO_ST-LSTM_(BO)_rpt.txt", evaluations_file="BO_ST-LSTM_(BO)_evals.txt")

opt_drive.x_opt