
from scipy.misc import toimage
import scipy.misc


import GPyOpt
import numpy as np
from keras.callbacks import EarlyStopping

from keras.models import Sequential
from keras.layers.core import Dense, Flatten
from keras.layers.convolutional import Convolution2D
from keras.optimizers import Adam
import time


#creiamo train e test con le rispettive immagini
start_time = time.time()
np.random.seed(42)

# creiamo train e test con le rispettive immagini

def conf_dir(env_key, default_value):
    return os.path.expanduser(os.getenv(env_key, default_value))


data_dir = conf_dir('PC_DATA_DIR', "~/work/cs/csp-drive-cl/data/def")


xs = []
ys = []

#read data.txt
with open(data_dir+"/driving_dataset/data.txt") as f:
    for line in f:
        xs.append(data_dir+"/driving_dataset/data_images/" + line.split()[0]) 
	# ci colleghiamo alla directory e aggiungiamo le immagini dato che sono numerate in modo sequenziale
        # the paper by Nvidia uses the inverse of the turning radius,
        # but steering wheel angle is proportional to the inverse of turning radius
        # so the steering wheel angle in radians is used as the output
        # ys.append(float(line.split()[1]) * scipy.pi / 180)
        line = line.replace(",", " ")
        ys.append(float(line.split()[1]) * scipy.pi / 180)

# get number of images
num_images = len(xs)

# shuffle list of images
c = list(zip(xs, ys))  # abbiamo abbinato la repository nel pc per ogni immagine con il corrispondente steering angle
c=c[0:39000]#39000
xs, ys = zip(*c)

train_xs=xs[:int(len(xs) * 0.8)]
train_ys=ys[:int(len(xs) * 0.8)]

test_xs = xs[-int(len(xs) * 0.2):]  # creazione del validation set
test_ys = ys[-int(len(xs) * 0.2):]

x_train = []
y_train = []
for i in range(0, len(train_xs)):
    x_train.append(scipy.misc.imresize(scipy.misc.imread(train_xs[i]), [66, 100]) / 255.0)  # reshape e normalizzazione
    y_train.append(np.array(train_ys[i]))

x_train = np.array(x_train)
y_train = np.array(y_train)

x_test = []
y_test = []
for i in range(0, len(test_xs)):
    x_test.append(scipy.misc.imresize(scipy.misc.imread(test_xs[i]), [66, 100]) / 255.0)  # reshape e normalizzazione
    y_test.append(test_ys[i])

x_test = np.array(x_test)
y_test = np.array(y_test)



# DRIVE class
class DRIVE():
    def __init__(self,  # first_input=784, last_output=10,
                 l1_out=24,
                 l2_out=36,
                 l3_out=48,
                 l4_out=64,
                 l5_out=64,
                 l6_out=100,
                 l7_out=50,
                 l8_out=10):
        # self.__first_input = first_input
        # self.__last_output = last_output
        self.l1_out = l1_out
        self.l2_out = l2_out
        self.l3_out = l3_out
        self.l4_out = l4_out
        self.l5_out = l5_out
        self.l6_out = l6_out
        self.l7_out = l7_out
        self.l8_out = l8_out
        self.__model = self.drive_model()

    def drive_model(self):
        model = Sequential()
        model.add(Convolution2D(self.l1_out, kernel_size=(5, 5), activation='elu', subsample=(2, 2), input_shape=(66, 100, 3)))
        model.add(Convolution2D(self.l2_out, kernel_size=(5, 5), activation='elu', subsample=(2, 2)))
        model.add(Convolution2D(self.l3_out, kernel_size=(5, 5), activation='elu', subsample=(2, 2)))
        model.add(Convolution2D(self.l4_out, kernel_size=(3, 3), activation='elu'))
        model.add(Convolution2D(self.l5_out, kernel_size=(3, 3), activation='elu'))
        model.add(Flatten())
        model.add(Dense(self.l6_out, activation='elu'))
        model.add(Dense(self.l7_out, activation='elu'))
        model.add(Dense(self.l8_out, activation='elu'))
        model.add(Dense(1))

        model.compile(loss='mse', optimizer=Adam())

        return model

    # fit mnist model
    def drive_fit(self):
        early_stopping = EarlyStopping(patience=0, verbose=1)

        self.__model.fit(x_train, y_train,
                         batch_size=100,
                         epochs=15,
                         verbose=1,
                         validation_split=0.2,
                         callbacks=[early_stopping])

    # evaluate mnist model
    def drive_evaluate(self):
        self.drive_fit()

        evaluation = self.__model.evaluate(x_test, y_test, verbose=1)
        return evaluation

# function to run mnist class
def run_drive(l1_out=24, l2_out=36, l3_out=48,l4_out=64,l5_out=64, l6_out=100, l7_out=50, l8_out=10):#first_input=784, last_output=10,
#l5_out=3
    _drive = DRIVE(l1_out=l1_out, l2_out=l2_out, l3_out=l3_out, l4_out=l4_out, l5_out=l5_out, l6_out=l6_out, l7_out=l7_out, l8_out=l8_out)
    drive_evaluation = _drive.drive_evaluate()
    return drive_evaluation

# bounds for hyper-parameters in mnist model
# the bounds dict should be in order of continuous type and then discrete type
bounds = [{'name': 'l1_out',          'type': 'discrete',  'domain': (24, 32, 48, 64)},
          {'name': 'l2_out',           'type': 'discrete',    'domain': (24, 32, 48, 64)},
          {'name': 'l3_out',           'type': 'discrete',    'domain': (24, 32, 48, 64)},
          {'name': 'l4_out',           'type': 'discrete',    'domain': (24, 32, 48, 64)},
          {'name': 'l5_out',           'type': 'discrete',    'domain': (24, 32, 48, 64)},
          {'name': 'l6_out',           'type': 'discrete',    'domain': (10, 50, 100)},
          {'name': 'l7_out',           'type': 'discrete',    'domain': (10, 50, 100)},
          {'name': 'l8_out',           'type': 'discrete',    'domain': (10, 50, 100)}]

# function to optimize mnist model
def f(x):
    print(x)
    evaluation = run_drive(
        l1_out = int(x[:,0]),
        l2_out = int(x[:,1]),
        l3_out = int(x[:,2]),
        l4_out = int(x[:,3]),
        l5_out = int(x[:,4]),
        l6_out = int(x[:,5]),
        l7_out = int(x[:,6]),
        l8_out = int(x[:,7]))
    print("LOSS:\t{0}".format(evaluation))
    print(evaluation)
    return evaluation

#Optimizer Istance
opt_drive=GPyOpt.methods.BayesianOptimization(f=f, domain=bounds, model_type="GP", acquisition_type="EI")
print(opt_drive)

#Optimize ST-LSTM Model
# Modified by Antonio to save BO's results (2020-04-18)
#opt_drive.run_optimization(max_iter=10)
opt_drive.run_optimization(max_iter=10, report_file="BO_PilotNet_rpt.txt", evaluations_file="BO_PilotNet_evals.txt")


print(opt_drive.x_opt)
print(opt_drive.fx_opt)

print(opt_drive.x_opt[0])
print(opt_drive.x_opt[1])
print(opt_drive.x_opt[2])
print(opt_drive.x_opt[3])
print(opt_drive.x_opt[4])
print(opt_drive.x_opt[5])
print(opt_drive.x_opt[6])
print(opt_drive.x_opt[7])

print("--- %s seconds ---" % (time.time() - start_time))
