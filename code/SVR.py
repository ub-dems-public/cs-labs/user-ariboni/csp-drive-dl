import pandas as pd
import numpy as np
import scipy.misc
from PIL import Image
from skimage.feature import hog
from skimage.color import rgb2grey
from sklearn.model_selection import train_test_split
from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error
from numpy import random
import time
start_time = time.time()
#creiamo train e test con le rispettive immagini
np.random.seed(42)

def conf_dir(env_key, default_value):
    return os.path.expanduser(os.getenv(env_key, default_value))


data_dir = conf_dir('PC_DATA_DIR', "~/work/cs/csp-drive-cl/data/def")


xs = []
ys = []

#read data.txt
with open(data_dir+"/driving_dataset/data.txt") as f:
    for line in f:
        xs.append(data_dir+"/driving_dataset/data_images/" + line.split()[0]) 
	# ci colleghiamo alla directory e aggiungiamo le immagini dato che sono numerate in modo sequenziale
        # the paper by Nvidia uses the inverse of the turning radius,
        # but steering wheel angle is proportional to the inverse of turning radius
        # so the steering wheel angle in radians is used as the output
        # ys.append(float(line.split()[1]) * scipy.pi / 180)
        line = line.replace(",", " ")
        ys.append(float(line.split()[1]) * scipy.pi / 180)
num_images = len(xs)

#shuffle list of images
c = list(zip(xs, ys))
c=c[0:39000]
#abbiamo abbinato la repository nel pc per ogni immagine con il corrispondente steering angle
#c=c[0:2000] #prendiamo un sottocampione che ci serve, diaciamo 2000
xs, ys = zip(*c) #riotteniamo le variabili che ci servono

#partizioniamo in train e test
train = c[:int(len(xs) * 0.8)]#0.8
test = c[-int(len(xs) * 0.2):]
print(len(train))

#Vogliamo creare una funzione che ci consente di passare da un array tridimensionale ad un array ad una dimensione (flat)
#dove l'array tridimensionale è l'immagine stessa

def create_features(img): #creiamo questa funzione che utilizzeremo nella prossima funzione
    # flatten three channel color image
    color_features = img.flatten() #Restituisce una copia dell'array collassato in una dimensione
    # convert image to greyscale
    grey_image = rgb2grey(img) #trasformiamo l'immagine in bianco e nero
    # get HOG features from greyscale image
    hog_features = hog(grey_image, block_norm='L2-Hys', pixels_per_cell=(16, 16)) #creiamo l'Histogram of oriented gradients
    # combine color and hog features into a single array
    flat_features = np.hstack(color_features)
    return flat_features

print("return flat_features")
# Dopo aver generato un array unidimensionale della singola immagine vogliamo ciclare su tutte le immagini. Creiamo features
# per ogni immagine e poi impiliamo l'array unidimensionale di features che il nostro modello può comprendere

def create_feature_matrix(
        label_dataframe):  # questa funzione ci consente di schiacciare in un unico vettore le informazioni della singola immagine e successivamente di impilarle per creare una matrice
    features_list = []

    for img_id in label_dataframe:
        # load image
        img = Image.open(img_id)  # Image.open perchè non funzionava get_image
        # ridimensiamo l'immagine in BN per ridurre i tempi computazionali
        reshaped_img = scipy.misc.imresize(img, [66, 100]) / 255.0  # reshape e normalizzazione
        # get features for image
        image_features = create_features(reshaped_img)
        features_list.append(image_features)

    # convert list of arrays into a matrix
    feature_matrix = np.array(features_list)
    return feature_matrix

print("return feature_matrix")

# run create_feature_matrix on our dataframe of images
feature_matrix = create_feature_matrix(np.array(train)[:, 0])
target = np.array(train)[:, 1]
print("feature map")
# creiamo il ricampionamento bootstrap

X=pd.DataFrame(feature_matrix)
y =pd.Series(target)
X_train, X_val, y_train, y_val = train_test_split(X,
                                                    y,
                                                    test_size=.2)

regressor = SVR(kernel='rbf')
regressor.fit(X_train, y_train)  # adattiamo il modello alle osservazioni bootrappate

y_pred_train=regressor.predict(X_train)
mse_train =mean_squared_error(y_train, y_pred_train)
print(len(X_train))
print("mse train")
print(mse_train)


y_pred_val = regressor.predict(X_val)  # facciamo la previsione sulle osservazioni bootstrappate
mse_val = mean_squared_error(y_val, y_pred_val)
print(len(X_val))# calcoliamo il mse
print("mse validation")
print(mse_val)

#Vediamo i risultati anche sul test

#Trasformiamo i dati di test affinchè siano comprensibili al modello
X_test = create_feature_matrix(np.array(test)[:,0])

#trasformiamo la target variable
y_test=[]
for ind in range (0, len(test)):
     y_test.append(float(np.array(test)[ind,1]))#necessaria per essere capita dal modello

#facciamo le previsioni sul test con il modello addestrato
y_pred_test = regressor.predict(X_test)
mse_test = mean_squared_error(y_test, y_pred_test)
print(len(X_test))
print("mse test")
print(mse_test)
print("--- %s seconds ---" % (time.time() - start_time))