import numpy as np
import scipy.misc
from numpy import random

from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Flatten
from keras.layers import BatchNormalization
from keras.layers import LeakyReLU
from keras.layers.convolutional_recurrent import ConvLSTM2D
from keras.layers import Conv3D, AveragePooling3D, MaxPooling3D
from keras.layers import TimeDistributed
import time
start_time = time.time()
np.random.seed(42)

# creiamo train e test con le rispettive immagini

def conf_dir(env_key, default_value):
    return os.path.expanduser(os.getenv(env_key, default_value))


data_dir = conf_dir('PC_DATA_DIR', "~/work/cs/csp-drive-cl/data/def")


xs = []
ys = []

#read data.txt
with open(data_dir+"/driving_dataset/data.txt") as f:
    for line in f:
        xs.append(data_dir+"/driving_dataset/data_images/" + line.split()[0]) 
	# ci colleghiamo alla directory e aggiungiamo le immagini dato che sono numerate in modo sequenziale
        # the paper by Nvidia uses the inverse of the turning radius,
        # but steering wheel angle is proportional to the inverse of turning radius
        # so the steering wheel angle in radians is used as the output
        # ys.append(float(line.split()[1]) * scipy.pi / 180)
        line = line.replace(",", " ")
        ys.append(float(line.split()[1]) * scipy.pi / 180)

# get number of images
num_images = len(xs)

# shuffle list of images
c = list(zip(xs, ys))  # abbiamo abbinato la repository nel pc per ogni immagine con il corrispondente steering angle
c=c[0:39000]#39000
xs, ys = zip(*c)

train_xs=xs[:int(len(xs) * 0.8)]
train_ys=ys[:int(len(xs) * 0.8)]

test_xs = xs[-int(len(xs) * 0.2):]  # creazione del validation set
test_ys = ys[-int(len(xs) * 0.2):]

x_train = []
y_train = []
for i in range(0, len(train_xs)):
    x_train.append(scipy.misc.imresize(scipy.misc.imread(train_xs[i]), [66, 100]) / 255.0)  # reshape e normalizzazione
    y_train.append(np.array(train_ys[i]))

x_train = np.array(x_train)
y_train = np.array(y_train)

x_test = []
y_test = []
for i in range(0, len(test_xs)):
    x_test.append(scipy.misc.imresize(scipy.misc.imread(test_xs[i]), [66, 100]) / 255.0)  # reshape e normalizzazione
    y_test.append(test_ys[i])

x_test = np.array(x_test)
y_test = np.array(y_test)


frames=3
#tot images=63825
#samples=63825/3=21275
#samples=x_train/frames=12765
print(x_train.shape)
x_train = x_train.reshape(10400, 3, 66, 100, 3)#10400
print(x_train.shape)

print(y_train.shape)
y_train=y_train.reshape(10400, 3)#10400
print(y_train.shape)
y_mean=[]
import statistics
for el in y_train:
    media=statistics.mean(el)
    y_mean.append(media)
y_mean=np.array(y_mean)
print(y_mean.shape)

model = Sequential()

model.add(ConvLSTM2D(32, kernel_size=(3, 3), input_shape=(3,66,100, 3),padding='same', return_sequences=True, activation='relu'))
model.add(BatchNormalization())
model.add(ConvLSTM2D(40, kernel_size=(3, 3), padding='same', return_sequences=True, activation='relu'))
model.add(BatchNormalization())
model.add(ConvLSTM2D(32, kernel_size=(3, 3), padding='same',return_sequences=True, activation='relu'))
model.add(BatchNormalization())
model.add(ConvLSTM2D(8, kernel_size=(3, 3), padding='same', return_sequences=True, activation='relu'))
model.add(BatchNormalization())

model.add(Conv3D(2, kernel_size=(3, 3, 3),activation="sigmoid",
                 padding='same', data_format='channels_last'))

model.add(MaxPooling3D(pool_size=(2, 2, 2), strides=None, padding='valid', data_format=None))

model.add(Flatten())
model.add(Dense(10, activation='relu'))#512
model.add(LeakyReLU(-0.2))
model.add(Dropout(0.15))
model.add(Dense(1, activation='relu'))

model.compile(loss='mse', optimizer="adam") #come ottimizzatore abbiamo impostato adam e come pedita mse
model.summary()


history = model.fit(x_train, y_mean, batch_size=100, epochs=15, verbose=1, validation_split=0.2)  # 15 epoche


print(x_train.shape)
x_test = x_test.reshape(2600, 3, 66, 100, 3)#2600
print(x_train.shape)

print(y_test.shape)
y_test=y_test.reshape(2600, 3)#2600
print(y_test.shape)
y_mean_test=[]
import statistics
for el in y_test:
    media_test=statistics.mean(el)
    y_mean_test.append(media_test)
y_mean_test=np.array(y_mean_test)
print(y_mean.shape)
score = model.evaluate(x_test, y_mean_test, verbose=1)

# batch size 100 e 30 epoche le manteniamo per tutte le reti
print(history)
print(score)
print("--- %s seconds ---" % (time.time() - start_time))