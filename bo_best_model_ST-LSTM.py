import numpy as np
from numpy import random
from keras.models import Sequential, Model
from keras.layers import BatchNormalization, Cropping2D, Conv2D, MaxPooling2D, MaxPooling3D, Dense, Dropout, Flatten, \
    Conv3D, ConvLSTM2D, LeakyReLU, Lambda, AveragePooling3D
from keras.callbacks import EarlyStopping, ModelCheckpoint
import matplotlib.pyplot as plt
from keras.optimizers import Adam
import keras
import scipy
import cv2
import os

#np.random.seed(42)

model_type = "GP"
acquisition_type = "MPI"

#MPI
best_conf = [0.34112841283763873,4,16,4,16,1,5,0.001]

best_path = "/home/uc33014/work/cs/csp-drive-dl/data/def/run_2/gpyopt/GP_MPI/gpyopt_model_run_15.h5"

def conf_dir(env_key, default_value):
    return os.path.expanduser(os.getenv(env_key, default_value))

data_dir = conf_dir('PC_DATA_DIR', "~/work/cs/csp-drive-dl/data/def")


def read_images(xs):
    x = []
    for i in range(0, len(xs)):
        x.append(cv2.resize(cv2.imread(xs[i])[100:-20], (200, 66)))
    return np.array(x)

def load_images(num_images):
    xs = []
    ys = []

    # read data.txt
    with open(data_dir + "/driving_dataset/data.txt") as f:
        for line in f:
            xs.append(data_dir + "/driving_dataset/data_images/" + line.split()[0])
            # the paper by Nvidia uses the inverse of the turning radius,
            # but steering wheel angle is proportional to the inverse of turning radius
            # so the steering wheel angle in radians is used as the output
            # ys.append(float(line.split()[1]) * scipy.pi / 180)
            line = line.replace(",", " ")
            ys.append(float(line.split()[1]) * scipy.pi / 180)

    xs_ys = list(zip(xs, ys))  # associate each image with its steering angle

    sample = xs_ys[0:num_images]  # sample of images to load
    xs, ys = zip(*sample)

    train_xs = xs[:int(len(xs) * 0.8)]
    train_ys = ys[:int(len(xs) * 0.8)]

    test_xs = xs[-int(len(xs) * 0.2):]
    test_ys = ys[-int(len(xs) * 0.2):]

    valid_xs = train_xs[-int(len(train_xs) * 0.2):]
    valid_ys = train_ys[-int(len(train_ys) * 0.2):]

    train_xs = train_xs[:int(len(train_xs) * 0.8)]
    train_ys = train_ys[:int(len(train_ys) * 0.8)]

    x_train = read_images(train_xs)
    y_train = np.array(train_ys)

    x_valid = read_images(valid_xs)
    y_valid = np.array(valid_ys)

    x_test = read_images(test_xs)
    y_test = np.array(test_ys)

    return x_train, y_train, x_valid, y_valid, x_test, y_test


def create_stlstm(input_shape, l1_out=16, l2_out=4, l3_out=16,l4_out=16,l5_out=1, l6_out=50,l1_drop=0.4692891197930165, lr_adam=0.001):
    model = Sequential()
    model.add(Lambda(lambda x: x / 255.0, input_shape=input_shape))
    model.add(ConvLSTM2D(l1_out, kernel_size=(3, 3), padding='same', return_sequences=True))
    model.add(BatchNormalization())
    model.add(ConvLSTM2D(l2_out, kernel_size=(3, 3), padding='same', return_sequences=True))
    model.add(BatchNormalization())
    model.add(ConvLSTM2D(l3_out, kernel_size=(3, 3), padding='same', return_sequences=True))
    model.add(BatchNormalization())
    model.add(ConvLSTM2D(l4_out, kernel_size=(3, 3), padding='same', return_sequences=True))
    model.add(BatchNormalization())

    model.add(Conv3D(l5_out, kernel_size=(3, 3, 3), padding='same', data_format='channels_last'))
    model.add(MaxPooling3D(pool_size=(2, 2, 2), strides=None, padding='valid', data_format=None))

    model.add(Flatten())
    model.add(Dense(l6_out))
    model.add(LeakyReLU(-0.2))
    model.add(Dropout(l1_drop))
    model.add(Dense(1))
    print(model.summary())

    model.compile(loss='mse', optimizer=Adam(learning_rate=lr_adam))

    return model

def training_model(num_images=39000, load_weights=False):
    print("Load images...")
    x_train, y_train, x_valid, y_valid, x_test, y_test = load_images(num_images)
    print("Done!")

    # preprocessing per mini training
    x_train = x_train.reshape(8320, 3, 66, 200, 3)
    x_valid = x_valid.reshape(2080, 3, 66, 200, 3)
    x_test = x_test.reshape(2600, 3, 66, 200, 3)
    y_train = y_train.reshape(8320, 3)
    y_valid = y_valid.reshape(2080, 3)
    y_test = y_test.reshape(2600, 3)
    y_train = y_train[:, 2]
    y_valid = y_valid[:, 2]
    y_test = y_test[:, 2]

    model = create_stlstm(x_train[0].shape, best_conf[1], best_conf[2], best_conf[3], best_conf[4], best_conf[5], best_conf[6], best_conf[0], best_conf[7])
    model_weights_path = best_path #data_dir + "/gpyopt/" + model_type + "_" + acquisition_type + "/gpyopt_model_run_" + str(best_run)+ ".h5"

    if load_weights == True:

        model.load_weights(model_weights_path)
        print("Results on train set (MSE): ", model.evaluate(x_train, y_train, verbose=0))
        print("Results on validation set (MSE): ", model.evaluate(x_valid, y_valid, verbose=0))
        print("Results on test set (MSE): ", model.evaluate(x_test, y_test, verbose=0))

        y_train_pred = model.predict(x_train)
        y_valid_pred = model.predict(x_valid)
        y_test_pred = model.predict(x_test)

        fig = plt.figure(figsize=(20, 10))
        axes = fig.add_axes([0.1, 0.1, 0.8, 0.8])

        y = np.concatenate([y_train, y_valid, y_test])
        y_pred = np.concatenate([y_train_pred, y_valid_pred, y_test_pred])
        count = range(0, len(y))
        axes.plot(count, y, label="Actual values")
        axes.plot(count, y_pred, label="Predicted values")
        axes.vlines(x=len(y_train), ymin=-6, ymax=6, label='validation set', colors="r")
        axes.vlines(x=(len(y_train) + len(y_valid)), ymin=-6, ymax=6, label='test set')
        axes.legend()
        plt.title("Actual vs Predicted" + model_type + "_" + acquisition_type + "BO ST-LSTM MPI")
        #plt.show()
        plt.savefig(data_dir + '/images/' + model_type + "_" + acquisition_type + '_bo_stlstm_MPI_best.png')


if __name__ == "__main__":
    os.environ["CUDA_VISIBLE_DEVICES"] = "1"
    training_model(num_images=39000, load_weights=True)