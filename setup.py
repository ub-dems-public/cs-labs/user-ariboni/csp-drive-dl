from setuptools import setup, find_packages

setup(
    name='csp-drive-dl',
    version='1.0.0',
    packages=find_packages('csp'),
    package_dir={'': 'csp'},
    url='https://gitlab.com/ub-dems/cs-labs/user-nghioldi/csp-drive-dl',
    license='',
    author='Nicolò Ghioldi',
    author_email='n.ghioldi@campus.unimib.it',
    install_requires=[
        'mypy-lang',
        'py',
        'numpy',
        'scipy',
        'matplotlib',
        'pandas'
    ],
    tests_requires=[
        'colorama',
        'pbr',
        'mock',
        'polling',
        'pluggy',
        'pytest',
        'atomicwrites',
        'attrs',
        'more-itertools',
        'six'
    ],
    description='csp drive dl'
)

