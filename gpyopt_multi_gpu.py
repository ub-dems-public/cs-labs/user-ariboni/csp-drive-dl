import numpy as np
from keras.models import Sequential, Model
from keras.layers import BatchNormalization, Cropping2D, Conv2D, MaxPooling2D, MaxPooling3D, Dense, Dropout, Flatten, \
    Conv3D, ConvLSTM2D, LeakyReLU, Lambda, AveragePooling3D
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.optimizers import Adam
import matplotlib.pyplot as plt
import keras
import tensorflow as tf
import random as python_random
import scipy
import cv2
import os
import GPy, GPyOpt
import time
from datetime import timedelta
from keras import backend as K

os.environ['PYTHONHASHSEED'] = '0'
num_opt = 3
seed = 41 + num_opt

def conf_dir(env_key, default_value):
    return os.path.expanduser(os.getenv(env_key, default_value))

data_dir = conf_dir('PC_DATA_DIR', "~/work/cs/csp-drive-dl/data/def")

def read_images(xs):
    x = []
    for i in range(0, len(xs)):
        x.append(cv2.resize(cv2.imread(xs[i])[100:-20], (200, 66)))
    return np.array(x)

def load_images(num_images):
    xs = []
    ys = []

    # read data.txt
    with open(data_dir + "/driving_dataset/data.txt") as f:
        for line in f:
            xs.append(data_dir + "/driving_dataset/data_images/" + line.split()[0])
            # the paper by Nvidia uses the inverse of the turning radius,
            # but steering wheel angle is proportional to the inverse of turning radius
            # so the steering wheel angle in radians is used as the output
            # ys.append(float(line.split()[1]) * scipy.pi / 180)
            line = line.replace(",", " ")
            ys.append(float(line.split()[1]) * scipy.pi / 180)

    xs_ys = list(zip(xs, ys))  # associate each image with its steering angle

    sample = xs_ys[0:num_images]  # sample of images to load
    xs, ys = zip(*sample)

    train_xs = xs[:int(len(xs) * 0.8)]
    train_ys = ys[:int(len(xs) * 0.8)]

    test_xs = xs[-int(len(xs) * 0.2):]
    test_ys = ys[-int(len(xs) * 0.2):]

    valid_xs = train_xs[-int(len(train_xs) * 0.2):]
    valid_ys = train_ys[-int(len(train_ys) * 0.2):]

    train_xs = train_xs[:int(len(train_xs) * 0.8)]
    train_ys = train_ys[:int(len(train_ys) * 0.8)]

    x_train = read_images(train_xs)
    y_train = np.array(train_ys)

    x_valid = read_images(valid_xs)
    y_valid = np.array(valid_ys)

    x_test = read_images(test_xs)
    y_test = np.array(test_ys)

    return x_train, y_train, x_valid, y_valid, x_test, y_test


# DRIVE class
class DRIVE():
    def __init__(self,
                 l1_out=8,
                 l2_out=8,
                 l3_out=8,
                 l4_out=8,
                 l5_out=1,
                 l6_out=10,
                 l1_drop=0.5,
                 lr_adam=0.001):

        self.l1_out = l1_out
        self.l2_out = l2_out
        self.l3_out = l3_out
        self.l4_out = l4_out
        self.l5_out = l5_out
        self.l6_out = l6_out
        self.l1_drop = l1_drop
        self.lr_adam= lr_adam

        strategy = tf.distribute.MirroredStrategy()
        print("Number of devices: {}".format(strategy.num_replicas_in_sync))
        with strategy.scope():
            self.__model = self.drive_model()

    def drive_model(self):
        model = Sequential()
        model.add(Lambda(lambda x: x / 255.0, input_shape=(3, 66, 200, 3)))
        model.add(ConvLSTM2D(self.l1_out, kernel_size=(3, 3), padding='same', return_sequences=True))
        model.add(BatchNormalization())
        model.add(ConvLSTM2D(self.l2_out, kernel_size=(3, 3), padding='same', return_sequences=True))
        model.add(BatchNormalization())
        model.add(ConvLSTM2D(self.l3_out, kernel_size=(3, 3), padding='same', return_sequences=True))
        model.add(BatchNormalization())
        model.add(ConvLSTM2D(self.l4_out, kernel_size=(3, 3), padding='same', return_sequences=True))
        model.add(BatchNormalization())

        model.add(Conv3D(self.l5_out, kernel_size=(3, 3, 3), padding='same', data_format='channels_last'))
        model.add(MaxPooling3D(pool_size=(2, 2, 2), strides=None, padding='valid', data_format=None))

        model.add(Flatten())
        model.add(Dense(self.l6_out))
        model.add(LeakyReLU(-0.2))
        model.add(Dropout(self.l1_drop))
        model.add(Dense(1))
        print(model.summary())

        model.compile(loss='mse', optimizer=Adam(learning_rate=self.lr_adam))

        return model

    # fit mnist model
    def drive_fit(self):
        global run
        model_weights_path = data_dir + "/run_" + str(num_opt) + "/gpyopt/" + model_type + "_" + acquisition_type + "/gpyopt_model_run_" + str(run) + ".h5"

        callbacks = [
            EarlyStopping(monitor='val_loss', patience=5, verbose=0),
            ModelCheckpoint(filepath=model_weights_path, monitor='val_loss', save_best_only=True, mode="min", verbose=0)
        ]

        if run > 4:
            self.__model.fit(x_train, y_train,
                             batch_size=50,
                             epochs=15,
                             verbose=1,
                             validation_data=(x_valid, y_valid),
                             shuffle=True,
                             callbacks=callbacks)

        self.__model.load_weights(model_weights_path)
        run = run + 1

    # evaluate mnist model
    def drive_evaluate(self):
        self.drive_fit()

        evaluation = self.__model.evaluate(x_valid, y_valid, verbose=1)
        return evaluation

# function to run mnist class
def run_drive(l1_out=8, l2_out=8, l3_out=8,l4_out=8,l5_out=1, l6_out=10,l1_drop=0.5, lr_adam=0.001):

    _drive = DRIVE(l1_out=l1_out, l2_out=l2_out, l3_out=l3_out, l4_out=l4_out, l5_out=l5_out, l6_out=l6_out,l1_drop=l1_drop, lr_adam=lr_adam)
    drive_evaluation = _drive.drive_evaluate()
    return drive_evaluation

# bounds for hyper-parameters in mnist model
# the bounds dict should be in order of continuous type and then discrete type
bounds = [{'name': 'l1_drop',          'type': 'continuous',  'domain': (0.0, 0.5)},
          {'name': 'l1_out',           'type': 'discrete',    'domain': (4,8,10,16)},
          {'name': 'l2_out',           'type': 'discrete',    'domain': (4,8,10,16)},
          {'name': 'l3_out',           'type': 'discrete',    'domain': (4,8,10, 16)},
          {'name': 'l4_out',           'type': 'discrete',    'domain': (4,8,10, 16)},
          {'name': 'l5_out',           'type': 'discrete',    'domain': (1,2,3)},
          {'name': 'l6_out',           'type': 'discrete',    'domain': (5, 10, 25, 50)},
          {'name': 'lr_adam',           'type': 'discrete',   'domain': (0.01, 0.001, 0.0001, 0.00001)}]


# function to optimize mnist model
def f(x):
    print(x)
    evaluation = run_drive(
        l1_drop = float(x[:,0]),
        l1_out = int(x[:,1]),
        l2_out = int(x[:,2]),
        l3_out = int(x[:,3]),
        l4_out = int(x[:,4]),
        l5_out = int(x[:,5]),
        l6_out = int(x[:,6]),
        lr_adam = float(x[:,7]))
    print("LOSS:\t{0}".format(evaluation))
    return evaluation

if __name__ == '__main__':


    x_train, y_train, x_valid, y_valid, x_test, y_test = load_images(39000)
    x_train = x_train.reshape(8320, 3, 66, 200, 3)
    x_valid = x_valid.reshape(2080, 3, 66, 200, 3)
    x_test = x_test.reshape(2600, 3, 66, 200, 3)
    y_train = y_train.reshape(8320, 3)
    y_valid = y_valid.reshape(2080, 3)
    y_test = y_test.reshape(2600, 3)
    y_train = y_train[:, 2]
    y_valid = y_valid[:, 2]
    y_test = y_test[:, 2]

    model_type = "GP"
    acquisition_types = ["MPI"] #["LCB", "EI","MPI"]


    if not os.path.isdir(data_dir + "/run_" + str(num_opt)):
        os.makedirs(data_dir + "/run_" + str(num_opt))

    for acquisition_type in acquisition_types:

        np.random.seed(seed)
        python_random.seed(seed)
        tf.random.set_seed(seed)

        if not os.path.isdir(data_dir + "/run_" + str(num_opt) + "/gpyopt/" + model_type + "_" + acquisition_type):
            os.makedirs(data_dir + "/run_" + str(num_opt) + "/gpyopt/" + model_type + "_" + acquisition_type)

        print("Starting optimization process with acquisition function: ", acquisition_type)
        run = 1

        #Optimizer Istance
        opt_drive=GPyOpt.methods.BayesianOptimization(f=f, domain=bounds, model_type=model_type, acquisition_type=acquisition_type, num_cores=6, normalize_Y=False, exact_fvalue=True)
        print(opt_drive)

        #Optimize ST-LSTM Model
        start_time = time.monotonic()
        opt_drive.run_optimization(max_iter=20,
                                   report_file="BO_ST-LSTM_rpt_" + model_type + "_" + acquisition_type + "_" + str(num_opt) + ".txt",
                                   evaluations_file="BO_ST-LSTM_evals_" + model_type + "_" + acquisition_type + "_" + str(num_opt) + ".txt")
        end_time = time.monotonic()
        print("Total time of optimisation: ", timedelta(seconds=end_time - start_time))

        K.clear_session()

