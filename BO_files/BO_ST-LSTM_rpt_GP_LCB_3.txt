----------------------------- GPyOpt Report file -----------------------------------
GPyOpt Version 1.2.6
Date and time:               Mon Sep  7 04:41:55 2020
Optimization completed:      YES, 25 samples collected.
Number initial samples:      5 
Tolerance:                   1e-08.
Optimization time:           17489.142334222794 seconds.

-------------------------------- Problem set up ------------------------------------
Problem name:                no_name
Problem dimension:           8
Number continuous variables  1
Number discrete variables    7
Number bandits               0
Noiseless evaluations:       False
Cost used:                   User defined cost
Constraints:                  False

------------------------------ Optimization set up ---------------------------------
Normalized outputs:          False
Model type:                  GP
Model update interval:       1
Acquisition type:            LCB
Acquisition optimizer:       lbfgs
Acquisition type:            LCB
Acquisition optimizer:       lbfgs
Evaluator type (batch size): sequential (1)
Cores used:                  6

--------------------------------- Summary ------------------------------------------
Value at minimum:            0.51950014
Best found minimum location: 2.37098562e-01 8.00000000e+00 4.00000000e+00 1.60000000e+01
 1.60000000e+01 2.00000000e+00 2.50000000e+01 1.00000000e-03
----------------------------------------------------------------------------------------------
