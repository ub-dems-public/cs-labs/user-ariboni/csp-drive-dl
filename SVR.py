import cv2
import numpy as np
import scipy
from numpy import random
from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error
import pickle
import os
np.random.seed(42)

np.random.seed(42)


def conf_dir(env_key, default_value):
    return os.path.expanduser(os.getenv(env_key, default_value))

data_dir = conf_dir('PC_DATA_DIR', "~/work/cs/csp-drive-dl/data/def")


def read_images_flatten(xs):
    x = []
    for i in range(0, len(xs)):
        x.append(cv2.resize(cv2.imread(xs[i])[100:-20], (200, 66)).flatten())
    return np.array(x)

def load_images(num_images):
    xs = []
    ys = []

    # read data.txt
    with open(data_dir + "/driving_dataset/data.txt") as f:
        for line in f:
            xs.append(data_dir + "/driving_dataset/data_images/" + line.split()[0])
            # the paper by Nvidia uses the inverse of the turning radius,
            # but steering wheel angle is proportional to the inverse of turning radius
            # so the steering wheel angle in radians is used as the output
            # ys.append(float(line.split()[1]) * scipy.pi / 180)
            line = line.replace(",", " ")
            ys.append(float(line.split()[1]) * scipy.pi / 180)

    xs_ys = list(zip(xs, ys))  # associate each image with its steering angle

    sample = xs_ys[0:num_images]  # sample of images to load
    xs, ys = zip(*sample)

    train_xs = xs[:int(len(xs) * 0.8)]
    train_ys = ys[:int(len(xs) * 0.8)]

    test_xs = xs[-int(len(xs) * 0.2):]
    test_ys = ys[-int(len(xs) * 0.2):]

    valid_xs = train_xs[-int(len(train_xs) * 0.2):]
    valid_ys = train_ys[-int(len(train_ys) * 0.2):]

    train_xs = train_xs[:int(len(train_xs) * 0.8)]
    train_ys = train_ys[:int(len(train_ys) * 0.8)]

    x_train = read_images_flatten(train_xs)
    y_train = np.array(train_ys)

    x_valid = read_images_flatten(valid_xs)
    y_valid = np.array(valid_ys)

    x_test = read_images_flatten(test_xs)
    y_test = np.array(test_ys)

    return x_train, y_train, x_valid, y_valid, x_test, y_test

def training_svr(num_images=39000, load_model=False):

    x_train, y_train, x_valid, y_valid, x_test, y_test = load_images(num_images=num_images)

    model = SVR(kernel='rbf')
    if load_model == True:
        model = pickle.load(open(filename, 'rb'))
    else:
        model.fit(x_train, y_train)
        filename = data_dir + '/weights/SVR_rbf.sav'
        pickle.dump(model, open(filename, 'wb'))

    y_pred_train = model.predict(x_train)
    mse_train = mean_squared_error(y_train, y_pred_train)
    print("mse training set: ", mse_train)
    y_pred_val = model.predict(x_valid)
    mse_val = mean_squared_error(y_valid, y_pred_val)
    print("mse validation set: ", mse_val)
    y_pred_test = model.predict(x_test)
    mse_test = mean_squared_error(y_test, y_pred_test)
    print("mse test set: ", mse_test)

if __name__ == "__main__":
    training_svr(load_model=False)